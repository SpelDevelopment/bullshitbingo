package spel.ru.bullshitbingo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;

import com.etsy.android.grid.StaggeredGridView;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.nostra13.universalimageloader.core.*;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends Activity implements View.OnClickListener{

    StaggeredGridView imageContainer;
    ImageLoader imageLoader;
    ArrayList<String> uris = new ArrayList<String>();
    private DisplayImageOptions options;
    Button reset;
    ImageAdapter adapter;
    float lastPosition = 0;
    float currentStep = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageContainer = (StaggeredGridView) findViewById(R.id.grid_view);
        imageContainer.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                float minStep = 25f;
                float alphaByStep = 0.05f;
                float currentPosition = getCurrentScrollPosition();
                float currentAlpha = reset.getAlpha();
                if(currentAlpha > 1){
                    currentAlpha = 1;
                }else if(currentAlpha < 0){
                    currentAlpha = 0;
                }

                currentStep += lastPosition - currentPosition;
                if(currentStep > minStep){
                    reset.setAlpha(currentAlpha + alphaByStep);
                    currentStep = 0;
                }else if(currentStep < minStep * -1){
                    reset.setAlpha(currentAlpha - alphaByStep);
                    currentStep = 0;
                }
                lastPosition = currentPosition;

                reset.setVisibility(reset.getAlpha() > 0 ? View.VISIBLE : View.GONE);

            }
        });

        reset = (Button) findViewById(R.id.resetButton);
        reset.setOnClickListener(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        uris = getUris();
        if(uris == null){
            uris = generateUris();
            saveUris(uris);
        }

        adapter = new ImageAdapter();

        imageContainer.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveUris(ArrayList<String> uris){

        try {
            FileOutputStream fos = this.openFileOutput("uris", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(uris);
            os.close();
        } catch (IOException w) {
            w.printStackTrace();
        }
    }

    private ArrayList<String> getUris(){

        ArrayList<String> uris;
        try {
            FileInputStream fis = this.openFileInput("uris");
            ObjectInputStream is = new ObjectInputStream(fis);
            uris = (ArrayList<String>) is.readObject();
            is.close();
            return uris;
        } catch (IOException w) {
            w.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

    private ArrayList<String> generateUris(){

        ArrayList<String> uris = new ArrayList<String>();

        for(int i = 0; i < 50; i++){

            int width = new Random().nextInt(924) + 100;
            int height = new Random().nextInt(924) + 100;
            String uri = "http://lorempixel.com/" +width + "/" + height;
            uris.add(uri);

        }

        return uris;

    }

    @Override
    public void onClick(View v) {

        uris = generateUris();
        saveUris(uris);
        adapter.notifyDataSetChanged();

    }

    private int getCurrentScrollPosition(){

        View view = imageContainer.getChildAt(0);
        return  -view.getTop() + imageContainer.getFirstVisiblePosition() * view.getHeight();

    }

    public class ImageAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        ImageAdapter() {
            inflater = LayoutInflater.from(MainActivity.this);
        }

        @Override
        public int getCount() {
            return uris.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.custom_image_view, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.imageView = (DynamicHeightImageView) view.findViewById(R.id.imageView);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            ImageLoader.getInstance()
                    .displayImage(uris.get(position), holder.imageView, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBar.setProgress(0);
                            holder.progressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });

            return view;
        }
    }

    static class ViewHolder {
        DynamicHeightImageView imageView;
        ProgressBar progressBar;
    }




}
